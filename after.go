// +----------------------------------------------------------------------
// |
// |
// |
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/18 5:32 下午
// +----------------------------------------------------------------------
package bootstrap

import (
	"gitee.com/common_go/logger"
	"gitee.com/common_go/producer"
)

type AfterServerStopFunc func()

func CloseLogger() AfterServerStopFunc {
	return func() {
		logger.Sync()
	}
}

func CloseProducer() AfterServerStopFunc {
	return func() {
		producer.Close()
		return
	}
}
