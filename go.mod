module gitee.com/common_go/bootstrap

go 1.15

require (
	gitee.com/common_go/config v0.0.1
	gitee.com/common_go/dbdao v0.0.1
	gitee.com/common_go/logger v0.0.1
	gitee.com/common_go/producer v0.0.1
	gitee.com/common_go/redisdao v0.0.1
	gitee.com/common_go/tools v0.0.1
)
